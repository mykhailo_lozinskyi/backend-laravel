<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $students = Student::get();
        return response()->json($students);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedStudent = $request->validate([
            'first_name' => 'required|regex:/^[A-Z][a-z]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Z][a-z]+$/|max:255',
            'gender' => 'required|string|in:M,F',
            'group' => 'required|string|max:255',
            'birthday' => 'required|string|max:255'
        ]);

        $student = new Student();
        $student->fill($validatedStudent);
        $student->save();

        return response()->json($student);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $student = Student::find($id);
        if(!$student) {
            return response()->json(["message"=>"Student not Found"], 404);
        }
        return response()->json($student);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
        if(!$student) {
            return response()->json(["message"=>"Student not Found"], 404);
        }

        $validatedStudent = $request->validate([
            'first_name' => 'required|regex:/^[A-Z][a-z]+$/|max:255',
            'last_name' => 'required|regex:/^[A-Z][a-z]+$/|max:255',
            'gender' => 'required|string|in:M,F',
            'group' => 'required|string|max:255',
            'birthday' => 'required|string|max:255'
        ]);

        $student->fill($validatedStudent);
        $student->save();

        return response()->json($student);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        $validatedIds = $request->validate([
            "ids" => "required|array",
            "ids.*" => "required|exists:students,id"
        ]);

        $deleted = Student::whereIn('id', $validatedIds['ids'])->delete();
        return response()->json(["message" => "$deleted students deleted"]);
    }
}
